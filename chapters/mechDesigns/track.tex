\documentclass[../../main.tex]{subfiles}
\graphicspath{ {../../media/designDocumentMedia/} }

\begin{document}
\section{Track}

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{trackMedia/bankedturn_strong_front}
    \caption{A banked turn with its support structure}
\end{figure}


\subsection{Overview}
\paragraph{}
This roller coaster is three-dimensional, meaning the track has bends and turns in many different directions.
In order to accomplish this, the track has to be made of a material that can be bent to create different twisting features.
The cart rides on two PVC tubes, which are connected to wooden supports.

\subsection{Materials and Construction Techniques}
\begin{description}
    \item[Wood] The support structure of the track will be made out of wood.
    \item[PVC] The rails are made of PVC because it is a strong thermoplastic that is compliant and easily available in tube form.
        \begin{description}
            \item[Bending PVC Pipes]
                PVC will be bent using a hot sand process.
                This is done by heating sand in a baking dish to 400$^{\circ}$F.
                The temperature can be measured using a laser thermometer.
                Once the sand is hot, one end of the PVC pipe is sealed using a PVC cap and the hot sand is poured into the tube using a funnel.
                Heat resistant gloves will be used when handling hot sand or PVC past this point.
                If necessary, the other end is also capped, and the PVC is left for a minute to let the plastic warm.
                At this point, the flexible PVC tube can be moved to the roller coaster support structures and bent into place.
                The PVC should not be pressed, rather allowed to sink into place to ensure that the outer surface stays perfectly round.
                After approximately 15 minutes, the PVC should no longer be flexible and the hot sand can be poured out.
                The laser thermometer can be used to measure when the PVC is safe to touch.
                Once the sand is out of the PVC, it can be removed and the PVC pipe can be installed on the ties using the procedure below.

            \item[Attaching PVC Rails to Wood Ties]
                A jig will be designed to make this process fast and repeatable.
                First, the support structure will be created with empty ties. Then PVC track segments will be bent to fit it.
                In order to join the PVC to the ties, a jig will be used to hold them together and drill holes precisely.
                Pilot holes will then be drilled through the PVC and into the tie.
                Finally, the jig will be removed and screws will be driven through the pipe into the tie.

            \item[Connecting Sections of PVC Track]
                The track is a continuous loop of PVC pipe, so the ends of multiple pipes must be joined together.
                To accomplish this, a insert is made by cutting a four inch long section of PVC pipe.
                Then, a sliver of the circumference is removed.
                Once again, a baking pan of sand is heated to 400 degrees F.
                The four-inch piece is placed into the hot sand for about 30 seconds to soften it.
                Once soft, using heat safe gloves, the PVC is squeezed to reduce its circumference, closing the gap in the circumference.
                Using PVC primer and cement, the insert is glued to the insides of two pipes, permanently joining the ends together.
                \begin{figure}[H]
                    \centering
                    \includegraphics[width=0.5\linewidth]{trackMedia/pvcJoinDiagram.png}
                    \caption{Connecting the ends of two PVC pipes with cement and an insert}
                \end{figure}
        \end{description}
    \item[Steel] Steel brackets will be used to connect the PVC pipe to the wooden support structure where forces are highest. Steel will be used because it is strong and easily accessible. Laser cutting and bending of the brackets will be outsourced to a manufacturer.
        \begin{description}
            \item[Laser Cutting] Since many identical steel brackets need to be made, they will be laser cut out of sheet metal to reduce manual labor.
            \item[Press Brake Bending] The laser cut sheet metal will then be bent using a press brake to make the bracket.
        \end{description}
\end{description}


\subsection{Rail Tie Attachment Design}
\paragraph{}
The most critical part of the track is where the PVC rail meets the wooden tie.
This is where all the load from the cart gets transferred from the rails to the support structure.
Due to the horizontal direction of the grain of the wood, extreme loads on the end have the capability to delaminate the wood and split it at the end.
To reinforce this area, steel brackets will be installed.

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{trackMedia/railtieassembled}
    \caption{View of the steel bracket to support PVC connections.}
\end{figure}
The folded steel bracket completely surrounds the end of the wood.
It is mounted using bolts that go through to the other side and into a nut on the end.
When tightened, these bolts compress the entire assembly, preventing the wood from delaminating.
The small hump folded into the bracket, combined with the screws that go through the pipe, support the PVC rail.
\paragraph{}
This bracket will be used for high load sections of the track, but is not necessary for other areas.
Since fabricating metal brackets is expensive, the rest of the track will be connected directly to the wood.
For this connection type, the wood will have a rounded cutout that the PVC fits tightly inside.
Then, three screws will be put in from the outside to connect the PVC to the wood.

\subsection{Rail Tie Attachment Testing}
\paragraph{}
We used rail tie testing data from Paul Gregg's (BYRC) PVC roller coasters to evaluate whether our tie design would work.
These are their results:

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{trackMedia/testing_methodology_1}\includegraphics[width=0.5\linewidth]{trackMedia/testing_methodology_2}
    \caption{This is apparatus that was used by BYRC to test his tie designs.}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{trackMedia/welded_bracket_test}\includegraphics[width=0.5\linewidth]{trackMedia/welded_bracket_test_2}
    \caption{These are the results of testing of two steel bracket designs which are similar to ours.}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{trackMedia/straight_to_wood}
    \caption{These are the results from attaching the PVC pipe directly to wood.}
\end{figure}

Based on these test results, we expect our metal brackets to support close to 2000 lbF.
They will be used in parts of the track where forces are highest.
For the rest, direct-to-wood was shown to withstand over 1000 lbF, more than enough for the low-load sections.
During normal operation, we expect each connection to see 300 lbF maximum.

\subsection{Adaptable Track Supports}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{trackMedia/bankedturn_strong_side.JPG}
    \includegraphics[width=0.5\linewidth]{trackMedia/bankedturn_strong_top.JPG}
    \caption{Side and top views of high-strength banked turn supports}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{trackMedia/bankedturn_color}
    \caption{Color coded example of the tiered support structure.}
\end{figure}

\paragraph{}
This is an example of the support structure for a banked turn.
For complex track segments that experience high loads like this, each tie needs to be supported individually and be positioned precisely.
To solve these issues a three-tiered system was developed.
\begin{description}
    \item[Tier 3 (blue)] This tier directly links the rail ties to the tier 2 section. This is a trapezoid of 2x4 lumber whose side lengths can be adjusted for exact positioning.
    \item[Tier 2 (green)] This is a rectangular structure that links tiers 1 and 3. It allows tier 3 structures to connect at any angle needed to support the track.
    \item[Tier 1 (red)] This supports the tier 2 structure and has long horizontal beams that lie on the ground. This includes any extra lumber that allows tier 2 to stay rigid enough to provide a base for tier 3 structures.
\end{description}

By introducing the Tier 2 compatibility layer, sections of track that are curved and move in 3 dimensions can be supported.
The variable shapes of tier 3 combined with the strength of tiers 1 and 2 creates a solution that is strong and precise.
Using simulation results, the design can be tailored to the forces on the track at each tie.

\subsection{Low Complexity Track Supports}
\begin{figure}[H] \centering
    \includegraphics[width=0.5\linewidth]{trackMedia/bankedturn_side.JPG}\includegraphics[width=0.5\linewidth]{trackMedia/bankedturn_top.JPG}
    \caption{Side and top views of low complexity banked turn supports}
\end{figure}
\paragraph{}
This is a less-complex version of the track supports which will be used for sections of track that have lower forces and/or are mostly two-dimensional.
These are wide structured A-frames with cross bracing in between. Stronger and more frequent supports can be placed where needed.
\end{document}
