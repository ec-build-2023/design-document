\documentclass[../../main.tex]{subfiles}

\graphicspath{ {../../media/designDocumentMedia/} }

\begin{document}
\section{Cart}
\includegraphics[width=\linewidth]{cartMedia/cart_full}

\subsection{Overview}
\paragraph{}
The cart is a passive mechanism which constrains the rider to the track and allows them to travel down it in a safe, controlled fashion.
The energy required to move the cart along the track is solely provided by gravitational potential from starting the coaster at the top of the tower.
Electrically powered motion is never used.
The cart is primarily constructed from welded steel box tubing, CNC machined parts, and purchased components.

\subsection{Materials and Construction Techniques}
\begin{description}
    \item[Steel] The structure of the cart will be made out of steel box tube. This tubing is extremely strong, and can be sourced, cut and joined easily.
        \begin{description}
            \item[Welding] Cut sections of steel box tube will be connected using TIG welding. This technique melts the steel together under an inert gas atmosphere, fusing them into one part. Weld joints have the same strength as the material they connect.
        \end{description}
    \item[Bolts and Weld Bungs] Certain parts of the cart need to be bolted to the steel frame. For bolt connections, weld bungs reinforce the tubing, preventing it from crushing when the bolts are tightened.
    \item[Aluminum] Aluminum is weaker than steel, but much lighter and easier to machine. It will be used for parts that are less strength critical but need a precise shape.
        \begin{description}
            \item[CNC Machining] Aluminum parts will be fabricated using CNC machining, this computer-controlled process allows for precise features.
        \end{description}

    \item[UHMWPE] The wheels of the cart will be made out of Ultra High Molecular Weight PolyEthylene.
        This is a slippery self-lubricating wear resistant plastic.
        These qualities are desirable for rollercoaster wheels. As the cart rolls down the track, the wheels will have to slide due to slop in the fitment between the track and the cart.
        With a material like UHMWPE, this slippage will not wear down the wheels or the track, as there is very little friction between them, and the plastic holds up well to abrasion.
        \begin{description}
            \item[Lathe Turning] Since the wheels are rotationally symmetric, they will be fabricated by turning them on a lathe.
        \end{description}

\end{description}

\subsection{Frame}
\paragraph{}
The frame is the main component of the cart. It is made of a steel box tubing, holds the seat and surrounds the rider.

\begin{figure}[H]
    \centering
    \includegraphics[width=1\linewidth]{cartMedia/Cartframe}
    \caption{Model of the welded steel frame}
\end{figure}

\subsection{Front and Back Bogie}
\paragraph{}
The bogies are made of a single piece of box tube with holes to mount various components.
Each bogie holds two wheel modules called trucks, and can swivel to follow the curvature of the track.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{cartMedia/frontbogie}\includegraphics[width=0.5\linewidth]{cartMedia/backbogie}
    \caption{Front and back bogies}
\end{figure}

\subsection{Truck}
\paragraph{}
Each truck has six wheels, two on the top, bottom and side.
This way, no matter what orientation the cart is in, wheels are always rolling on the track.
It is physically impossible to remove the cart from the track when in use.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{cartMedia/real_trucks}\includegraphics[width=0.3\linewidth]{cartMedia/truck_wheels_transparent}
    \caption{\textbf{Left:} trucks of an amusement park roller coaster (shutterstock) \textbf{Right:} our design}
\end{figure}

% REMINDER: Source: shutterstock

\subsection{Wheels}
\paragraph{}
All of the cart's wheels are identical.
They are machined from a single rod of UHMWPE plastic, making them wear resistant and strong.
The plastic outer shell runs on two bearings, which themselves run on a steel spacer surrounding a solid steel bolt.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{cartMedia/uhmwWheelAssembly}
    \caption{UHMWPE Wheel Assembly, plastic in blue, axle and bearings in grey}
\end{figure}

\subsection{Cart: Loads and Forces}
\paragraph{}
The cart will weigh about 50 lbs.
We will only allow riders weighing a maximum of 250 lbs for a combined weight of 300 lbs.
As design goals, we defined some acceleration limits which will never be reached in normal operation.
\paragraph{}
\textbf{The numbers are as follows:} \\
Acceleration straight down: 4 G = 1200 lbF \\
Acceleration straight up (hanging upside down): 1.67 G = 500 lbF \\
Acceleration sideways: 1 G = 300 lbF \\

\subsection{Load Ratings}
\paragraph{}
We chose components designed to handle well above these theoretical maximum forces.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{cartMedia/truck_transparent}
    \caption{Load ratings of truck components}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{cartMedia/backbogie_diagram}
    \caption{Load ratings of back bogie components}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{cartMedia/frontbogie_diagram}
    \caption{Load ratings of front bogie components}
\end{figure}

\subsection{Load Analyses}
\paragraph{}
The images below are the results from static stress studies on various components.
All of the pictures show true-to-scale deformation, and are colored to indicate stress on the material.
Blue means little to no stress, and red is the yield stress of the material, where it will deform permanently.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{cartMedia/truckframe_rightsideup}\includegraphics[width=0.5\linewidth]{cartMedia/truckframe_upsidedown}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{cartMedia/truckframe_side}\includegraphics[width=0.5\linewidth]{cartMedia/backbogiebar_load}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{cartMedia/plasticWheel_load}
\end{figure}

\subsection{Safety Overview}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{cartMedia/sketch_side_with_restraint}\includegraphics[width=0.5\linewidth]{cartMedia/sketch_top_with_restraint}
    \caption{Side and top view drawings of cart with safety features in green}
\end{figure}

\paragraph{}
The mechanical design of the cart is more than strong enough to withstand any forces it experiences during the ride.
The cart also has several safety features:

\begin{description}
    \item[Steel frame] While seated in the cart, the rider is surrounded by a strong tube steel frame, which keeps them protected.
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.5\linewidth]{cartMedia/sketch_top_with_person}
            \caption{Sketch of cart frame}
        \end{figure}
    \item[Seat] Riders will sit in a bucket-style racing seat which will support their head, back and hips.
    \item[Footloops] To prevent riders' legs from swinging around outside the cart, their feet will be tucked into adjustable footloops.
    \item[Five Point Harness (Restraint 1)] Riders will be strapped into the chair with a five point racing harness.
        These harnesses are made out of high-strength webbing and crash-rated.
        They are also designed to keep race car drivers in their seats and able to reach the controls even in crashes or rolled cars.
        A harness like this is more than enough to protect the rider through all track elements, including upside-down segments.
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.5\linewidth]{cartMedia/5pt_harness}
            \caption{Attachment points of a five-point harness}
        \end{figure}
    \item[Lap Bar (Restraint 2)] A foam-padded steel bar that closes over the rider's lap after the harness is tightened.
        The lap bar is a redundant restraint that can keep the rider in the cart even without a harness.
        It will also serve as something the rider can grab to keep their arms within the cart.
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.5\linewidth]{cartMedia/lapbar}
            \caption{Closeup of lap bar}
        \end{figure}
    \item[Helmet] Riders will be required to wear helmets to protect their heads.
\end{description}

\end{document}
